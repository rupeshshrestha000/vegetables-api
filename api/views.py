from .models import Vegetable, Fruit
from .serializer import VegetableSerializer, FruitSerializer
from rest_framework import generics

class VegetableAPIView(generics.ListCreateAPIView):
    serializer_class = VegetableSerializer
    queryset = Vegetable.objects.all()


class VegetableAPIRetriveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = VegetableSerializer
    queryset = Vegetable.objects.all()


class FruitAPIView(generics.ListCreateAPIView):
    serializer_class = FruitSerializer
    queryset = Fruit.objects.all()


class FruitAPIRetriveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = FruitSerializer
    queryset = Fruit.objects.all()

# class VegetableAPIView(APIView):

    # def get(self, request):
    #     queryset = Vegetable.objects.all()
    #     serializer = VegetableSerializer(queryset, many=True)
    #     return Response(serializer.data)

    # def post(self, request):
    #     serializer = VegetableSerializer(data=request.data)

    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
