from django.db import models

# Choose month
MONTH_CHOICES = (
    ('baisakh','BAISAKH'),
    ('jesth', 'JESTH'),
    ('asar','ASAR'),
    ('shrawan','SHRAWAN'),
    ('bhadra','BHADRA'),
    ('aswin','ASWIN'),
    ('kartik','KARTIK'),
    ('mangsir','MANGSIR'),
    ('poush','POUSH'),
    ('magh','MAGH'),
    ('falgun','FALGUN'),
    ('chaitra','CHAITRA'),
)

# Create your models here.
class Vegetable(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images/')
    selectMonth = models.CharField(max_length=12, choices=MONTH_CHOICES, default='baisakh')
    daysForGermination = models.IntegerField()
    context = models.TextField(blank=True)
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class Fruit(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='images/')
    selectMonth = models.CharField(max_length=12, choices=MONTH_CHOICES, default='baisakh')
    daysForGermination = models.IntegerField()
    context = models.TextField(blank=True)
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title