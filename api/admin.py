from django.contrib import admin
from .models import Vegetable, Fruit

admin.site.register(Vegetable)
admin.site.register(Fruit)