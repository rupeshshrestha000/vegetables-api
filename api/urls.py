from django.urls import path
from .views import VegetableAPIView, VegetableAPIRetriveUpdateDestroy, FruitAPIView, FruitAPIRetriveUpdateDestroy

urlpatterns = [
    # path('vegetable/', VegetableAPIView.as_view(), name = "api-vegetables"),
    path('vegetable/', VegetableAPIView.as_view()),
    path('vegetable/<int:pk>/', VegetableAPIRetriveUpdateDestroy.as_view()),
    path('fruit/', FruitAPIView.as_view()),
    path('fruit/<int:pk>/', FruitAPIRetriveUpdateDestroy.as_view()),
]